resource_resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'


--------------------------------------------------------------------
--                     Veiculos de Trabalhos                      --
--------------------------------------------------------------------

--------------------------------------------------------------------

--Onibus------------------------------------------------------------
--[[--Comum de Rua 
data_file 'HANDLING_FILE' 'data/Carros/s10/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/s10/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/s10/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/s10/carvariations.meta'

files {
'data/Carros/s10/handling.meta',
'data/Carros/s10/vehicles.meta',
'data/Carros/s10/carcols.meta',
'data/Carros/s10/carvariations.meta',
}
--Comum - De rua baseado no transit
data_file 'HANDLING_FILE' 'data/Carros/s10/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/s10/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/s10/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/s10/carvariations.meta'

files {
'data/Carros/s10/handling.meta',
'data/Carros/s10/vehicles.meta',
'data/Carros/s10/carcols.meta',
'data/Carros/s10/carvariations.meta',
}
--Viagem -- 
data_file 'HANDLING_FILE' 'data/Carros/s10/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/s10/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/s10/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/s10/carvariations.meta'

files {
'data/Carros/s10/handling.meta',
'data/Carros/s10/vehicles.meta',
'data/Carros/s10/carcols.meta',
'data/Carros/s10/carvariations.meta',
}
---Viagem 2 - 
data_file 'HANDLING_FILE' 'data/Carros/s10/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/s10/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/s10/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/s10/carvariations.meta'

files {
'data/Carros/s10/handling.meta',
'data/Carros/s10/vehicles.meta',
'data/Carros/s10/carcols.meta',
'data/Carros/s10/carvariations.meta',
}
--------------------------------------------------------------------
--]]
--Lixeiro-----------------------------------------------------------

---Caminhao de Lixo 1 - Addon modelo mercedez benz 
data_file 'HANDLING_FILE' 'data/Trabalhos/Lixeiro/Lixo/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Trabalhos/Lixeiro/Lixo/vehicles.meta'

files {
'data/Trabalhos/Lixeiro/Lixo/handling.meta',
'data/Trabalhos/Lixeiro/Lixo/vehicles.meta',
}
---Caminhao de Lixo 2 - Eu amo São Paulo - 2017 Doria - Modificado apartir do game 
data_file 'HANDLING_FILE' 'data/Trabalhos/Lixeiro/Lixo2/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Trabalhos/Lixeiro/Lixo2/vehicles.meta'

files {
'data/Trabalhos/Lixeiro/Lixo2/handling.meta',
'data/Trabalhos/Lixeiro/Lixo2/vehicles.meta',
}












































-----------------------------CARROS----------------------------------

data_file 'HANDLING_FILE' 'data/Carros/s10/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/s10/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/s10/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/s10/carvariations.meta'

files {
'data/Carros/s10/handling.meta',
'data/Carros/s10/vehicles.meta',
'data/Carros/s10/carcols.meta',
'data/Carros/s10/carvariations.meta',
}

---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/Carros/golfsportline/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/golfsportline/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/golfsportline/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/golfsportline/carvariations.meta'

files {
'data/Carros/golfsportline/handling.meta',
'data/Carros/golfsportline/vehicles.meta',
'data/Carros/golfsportline/carcols.meta',
'data/Carros/golfsportline/carvariations.meta',
}
--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/saveirog5/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/saveirog5/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/saveirog5/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/saveirog5/carvariations.meta'

files {
'data/Carros/saveirog5/handling.meta',
'data/Carros/saveirog5/vehicles.meta',
'data/Carros/saveirog5/carcols.meta',
'data/Carros/saveirog5/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/passat/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/passat/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/passat/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/passat/carvariations.meta'

files {
'data/Carros/passat/handling.meta',
'data/Carros/passat/vehicles.meta',
'data/Carros/passat/carcols.meta',
'data/Carros/passat/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/hilux/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/hilux/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/hilux/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/hilux/carvariations.meta'

files {
'data/Carros/hilux/handling.meta',
'data/Carros/hilux/vehicles.meta',
'data/Carros/hilux/carcols.meta',
'data/Carros/hilux/carvariations.meta',
}

--------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/Carros/civiclxl/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/civiclxl/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/civiclxl/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/civiclxl/carvariations.meta'

files {
'data/Carros/civiclxl/handling.meta',
'data/Carros/civiclxl/vehicles.meta',
'data/Carros/civiclxl/carcols.meta',
'data/Carros/civiclxl/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/civicsedan/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/civicsedan/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/civicsedan/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/civicsedan/carvariations.meta'

files {
'data/Carros/civicsedan/handling.meta',
'data/Carros/civicsedan/vehicles.meta',
'data/Carros/civicsedan/carcols.meta',
'data/Carros/civicsedan/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/spacefox/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/spacefox/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/spacefox/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/spacefox/carvariations.meta'

files {
'data/Carros/spacefox/handling.meta',
'data/Carros/spacefox/vehicles.meta',
'data/Carros/spacefox/carcols.meta',
'data/Carros/spacefox/carvariations.meta',
}


--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/fiatbravo/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/fiatbravo/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/fiatbravo/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/fiatbravo/carvariations.meta'

files {
'data/Carros/fiatbravo/handling.meta',
'data/Carros/fiatbravo/vehicles.meta',
'data/Carros/fiatbravo/carcols.meta',
'data/Carros/fiatbravo/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/golgl/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/golgl/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/golgl/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/golgl/carvariations.meta'

files {
'data/Carros/golgl/handling.meta',
'data/Carros/golgl/vehicles.meta',
'data/Carros/golgl/carcols.meta',
'data/Carros/golgl/carvariations.meta',
} 

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/astragsi/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/astragsi/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/astragsi/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/astragsi/carvariations.meta'

files {
'data/Carros/astragsi/handling.meta',
'data/Carros/astragsi/vehicles.meta',
'data/Carros/astragsi/carcols.meta',
'data/Carros/astragsi/carvariations.meta',
} 


--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/rs7/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/rs7/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/rs7/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/rs7/carvariations.meta'

files {
'data/Carros/rs7/handling.meta',
'data/Carros/rs7/vehicles.meta',
'data/Carros/rs7/carcols.meta',
'data/Carros/rs7/carvariations.meta',
}
 
 --------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/370z/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/370z/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/370z/carvariations.meta'

files {
'data/Carros/370z/handling.meta',
'data/Carros/370z/vehicles.meta',
'data/Carros/370z/carvariations.meta',
}
 
--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/lwgtr/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/lwgtr/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/lwgtr/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/lwgtr/carvariations.meta'

files {
'data/Carros/lwgtr/handling.meta',
'data/Carros/lwgtr/vehicles.meta',
'data/Carros/lwgtr/carcols.meta',
'data/Carros/lwgtr/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/nissanqashqai16/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/nissanqashqai16/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/nissanqashqai16/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/nissanqashqai16/carvariations.meta'

files {
'data/Carros/nissanqashqai16/handling.meta',
'data/Carros/nissanqashqai16/vehicles.meta',
'data/Carros/nissanqashqai16/carcols.meta',
'data/Carros/nissanqashqai16/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/audisq7/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/audisq7/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/audisq7/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/audisq7/carvariations.meta'

files {
'data/Carros/audisq7/handling.meta',
'data/Carros/audisq7/vehicles.meta',
'data/Carros/audisq7/carcols.meta',
'data/Carros/audisq7/carvariations.meta',
}


--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/pturismo/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/pturismo/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/pturismo/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/pturismo/carvariations.meta'

files {
'data/Carros/pturismo/handling.meta',
'data/Carros/pturismo/vehicles.meta',
'data/Carros/pturismo/carcols.meta',
'data/Carros/pturismo/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/208/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/208/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/208/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/208/carvariations.meta'

files {
'data/Carros/208/handling.meta',
'data/Carros/208/vehicles.meta',
'data/Carros/208/carcols.meta',
'data/Carros/208/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/santafe/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/santafe/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/santafe/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/santafe/carvariations.meta'

files {
'data/Carros/santafe/handling.meta',
'data/Carros/santafe/vehicles.meta',
'data/Carros/santafe/carcols.meta',
'data/Carros/santafe/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/mercedesslr/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/mercedesslr/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/mercedesslr/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/mercedesslr/carvariations.meta'

files {
'data/Carros/mercedesslr/handling.meta',
'data/Carros/mercedesslr/vehicles.meta',
'data/Carros/mercedesslr/carcols.meta',
'data/Carros/mercedesslr/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/mazda/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/mazda/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/mazda/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/mazda/carvariations.meta'

files {
'data/Carros/mazda/handling.meta',
'data/Carros/mazda/vehicles.meta',
'data/Carros/mazda/carcols.meta',
'data/Carros/mazda/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/fusca/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/fusca/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/fusca/carvariations.meta'

files {
'data/Carros/fusca/handling.meta',
'data/Carros/fusca/vehicles.meta',
'data/Carros/fusca/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/toro/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/toro/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/toro/carvariations.meta'

files {
'data/Carros/toro/handling.meta',
'data/Carros/toro/vehicles.meta',
'data/Carros/toro/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/carropizza/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/carropizza/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/carropizza/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/carropizza/carvariations.meta'

files {
'data/Carros/carropizza/handling.meta',
'data/Carros/carropizza/vehicles.meta',
'data/Carros/carropizza/carcols.meta',
'data/Carros/carropizza/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/masseratilevante/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/masseratilevante/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/masseratilevante/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/masseratilevante/carvariations.meta'

files {
'data/Carros/masseratilevante/handling.meta',
'data/Carros/masseratilevante/vehicles.meta',
'data/Carros/masseratilevante/carcols.meta',
'data/Carros/masseratilevante/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/masseratighibli/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/masseratighibli/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/masseratighibli/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/masseratighibli/carvariations.meta'

files {
'data/Carros/masseratighibli/handling.meta',
'data/Carros/masseratighibli/vehicles.meta',
'data/Carros/masseratighibli/carcols.meta',
'data/Carros/masseratighibli/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/audir8/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/audir8/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/audir8/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/audir8/carvariations.meta'

files {
'data/Carros/audir8/handling.meta',
'data/Carros/audir8/vehicles.meta',
'data/Carros/audir8/carcols.meta',
'data/Carros/audir8/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/porscheboxster/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/porscheboxster/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/porscheboxster/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/porscheboxster/carvariations.meta'

files {
'data/Carros/porscheboxster/handling.meta',
'data/Carros/porscheboxster/vehicles.meta',
'data/Carros/porscheboxster/carcols.meta',
'data/Carros/porscheboxster/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/zentenario/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/zentenario/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/zentenario/carvariations.meta'

files {
'data/Carros/zentenario/handling.meta',
'data/Carros/zentenario/vehicles.meta',
'data/Carros/zentenario/carvariations.meta',
}


--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/touareg/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/touareg/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/touareg/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/touareg/carvariations.meta'

files {
'data/Carros/touareg/handling.meta',
'data/Carros/touareg/vehicles.meta',
'data/Carros/touareg/carcols.meta',
'data/Carros/touareg/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/veloster/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/veloster/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/veloster/carvariations.meta'

files {
'data/Carros/veloster/handling.meta',
'data/Carros/veloster/vehicles.meta',
'data/Carros/veloster/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/ss2016/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/ss2016/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/ss2016/carvariations.meta'

files {
'data/Carros/ss2016/handling.meta',
'data/Carros/ss2016/vehicles.meta',
'data/Carros/ss2016/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/gtoxx/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/gtoxx/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/gtoxx/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/gtoxx/carvariations.meta'

files {
'data/Carros/gtoxx/handling.meta',
'data/Carros/gtoxx/vehicles.meta',
'data/Carros/gtoxx/carcols.meta',
'data/Carros/gtoxx/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/mustang/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/mustang/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/mustang/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/mustang/carvariations.meta'

files {
'data/Carros/mustang/handling.meta',
'data/Carros/mustang/vehicles.meta',
'data/Carros/mustang/carcols.meta',
'data/Carros/mustang/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/bmwx6/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/bmwx6/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/bmwx6/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/bmwx6/carvariations.meta'

files {
'data/Carros/bmwx6/handling.meta',
'data/Carros/bmwx6/vehicles.meta',
'data/Carros/bmwx6/carcols.meta',
'data/Carros/bmwx6/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/rangerover/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/rangerover/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/rangerover/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/rangerover/carvariations.meta'

files {
'data/Carros/rangerover/handling.meta',
'data/Carros/rangerover/vehicles.meta',
'data/Carros/rangerover/carcols.meta',
'data/Carros/rangerover/carvariations.meta'
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/dodge2018/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/dodge2018/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/dodge2018/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/dodge2018/carvariations.meta'

files {
'data/Carros/dodge2018/handling.meta',
'data/Carros/dodge2018/vehicles.meta',
'data/Carros/dodge2018/carcols.meta',
'data/Carros/dodge2018/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/i8/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/i8/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/i8/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/i8/carvariations.meta'

files {
'data/Carros/i8/handling.meta',
'data/Carros/i8/vehicles.meta',
'data/Carros/i8/carcols.meta',
'data/Carros/i8/carvariations.meta',
}

--------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Carros/sextoelemento/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Carros/sextoelemento/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Carros/sextoelemento/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Carros/sextoelemento/carvariations.meta'

files {
'data/Carros/sextoelemento/handling.meta',
'data/Carros/sextoelemento/vehicles.meta',
'data/Carros/sextoelemento/carcols.meta',
'data/Carros/sextoelemento/carvariations.meta'
}











------------------------------Motos-------------------------------------


data_file 'HANDLING_FILE' 'data/Motos/h2r/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/h2r/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/h2r/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/h2r/carvariations.meta'

files {
'data/Motos/h2r/handling.meta',
'data/Motos/h2r/vehicles.meta',
'data/Motos/h2r/carcols.meta',
'data/Motos/h2r/carvariations.meta',
}

---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/gsxr1000/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/gsxr1000/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/gsxr1000/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/gsxr1000/carvariations.meta'

files {
'data/Motos/gsxr1000/handling.meta',
'data/Motos/gsxr1000/vehicles.meta',
'data/Motos/gsxr1000/carcols.meta',
'data/Motos/gsxr1000/carvariations.meta',
}

---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/r12014/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/r12014/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/r12014/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/r12014/carvariations.meta'

files {
'data/Motos/r12014/handling.meta',
'data/Motos/r12014/vehicles.meta',
'data/Motos/r12014/carcols.meta',
'data/Motos/r12014/carvariations.meta',
}

---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/factor125/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/factor125/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/factor125/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/factor125/carvariations.meta'

files {
'data/Motos/factor125/handling.meta',
'data/Motos/factor125/vehicles.meta',
'data/Motos/factor125/carcols.meta',
'data/Motos/factor125/carvariations.meta',
}

---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/diavel/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/diavel/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/diavel/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/diavel/carvariations.meta'

files {
'data/Motos/diavel/handling.meta',
'data/Motos/diavel/vehicles.meta',
'data/Motos/diavel/carcols.meta',
'data/Motos/diavel/carvariations.meta',
}


---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/dm1200/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/dm1200/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/dm1200/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/dm1200/carvariations.meta'

files {
'data/Motos/dm1200/handling.meta',
'data/Motos/dm1200/vehicles.meta',
'data/Motos/dm1200/carcols.meta',
'data/Motos/dm1200/carvariations.meta',
}

---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/confederate/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/confederate/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/confederate/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/confederate/carvariations.meta'

files {
'data/Motos/confederate/handling.meta',
'data/Motos/confederate/vehicles.meta',
'data/Motos/confederate/carcols.meta',
'data/Motos/confederate/carvariations.meta',
}

---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/s1000/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/s1000/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/s1000/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/s1000/carvariations.meta'

files {
'data/Motos/s1000/handling.meta',
'data/Motos/s1000/vehicles.meta',
'data/Motos/s1000/carcols.meta',
'data/Motos/s1000/carvariations.meta',
}


---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/hayabusa/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/hayabusa/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/hayabusa/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/hayabusa/carvariations.meta'

files {
'data/Motos/hayabusa/handling.meta',
'data/Motos/hayabusa/vehicles.meta',
'data/Motos/hayabusa/carcols.meta',
'data/Motos/hayabusa/carvariations.meta',
}


---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/panigale/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/panigale/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/panigale/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/panigale/carvariations.meta'

files {
'data/Motos/panigale/handling.meta',
'data/Motos/panigale/vehicles.meta',
'data/Motos/panigale/carcols.meta',
'data/Motos/panigale/carvariations.meta',
}

---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/cbcarenada/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/cbcarenada/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/cbcarenada/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/cbcarenada/carvariations.meta'

files {
'data/Motos/cbcarenada/handling.meta',
'data/Motos/cbcarenada/vehicles.meta',
'data/Motos/cbcarenada/carcols.meta',
'data/Motos/cbcarenada/carvariations.meta',
}


---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/yzfr6/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/yzfr6/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/yzfr6/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/yzfr6/carvariations.meta'

files {
'data/Motos/yzfr6/handling.meta',
'data/Motos/yzfr6/vehicles.meta',
'data/Motos/yzfr6/carcols.meta',
'data/Motos/yzfr6/carvariations.meta',
}

---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/augusta/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/augusta/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/augusta/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/augusta/carvariations.meta'

files {
'data/Motos/augusta/handling.meta',
'data/Motos/augusta/vehicles.meta',
'data/Motos/augusta/carcols.meta',
'data/Motos/augusta/carvariations.meta',
}

---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/z1000/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/z1000/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/z1000/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/z1000/carvariations.meta'

files {
'data/Motos/z1000/handling.meta',
'data/Motos/z1000/vehicles.meta',
'data/Motos/z1000/carcols.meta',
'data/Motos/z1000/carvariations.meta',
}

---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/hondabiz/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/hondabiz/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/hondabiz/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/hondabiz/carvariations.meta'

files {
'data/Motos/hondabiz/handling.meta',
'data/Motos/hondabiz/vehicles.meta',
'data/Motos/hondabiz/carcols.meta',
'data/Motos/hondabiz/carvariations.meta',
}

---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/ktm/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/ktm/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/ktm/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/ktm/carvariations.meta'

files {
'data/Motos/ktm/handling.meta',
'data/Motos/ktm/vehicles.meta',
'data/Motos/ktm/carcols.meta',
'data/Motos/ktm/carvariations.meta',
}

---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/xt66/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/xt66/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Motos/xt66/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/xt66/carvariations.meta'

files {
'data/Motos/xt66/handling.meta',
'data/Motos/xt66/vehicles.meta',
'data/Motos/xt66/carcols.meta',
'data/Motos/xt66/carvariations.meta',
}

---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Motos/bros/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Motos/bros/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Motos/bros/carvariations.meta'

files {
'data/Motos/bros/handling.meta',
'data/Motos/bros/vehicles.meta',
'data/Motos/bros/carvariations.meta'
}
--[[
--------------------------------MicroOnibus---------------------------------

data_file 'HANDLING_FILE' 'data/Viaturas/MicroOnibus/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Viaturas/MicroOnibus/vehicles.meta'

files {
'data/Viaturas/MicroOnibus/handling.meta',
'data/Viaturas/MicroOnibus/vehicles.meta'
}

--]]
----------------TEMPORARIO TEMPORARIO

data_file 'HANDLING_FILE' 'data/Temporario/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Temporario/vehicles.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Temporario/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Temporario/carvariations.meta'

files {
'data/Temporario/handling.meta',
'data/Temporario/vehicles.meta',
'data/Temporario/carvariations.meta',
'data/Temporario/vehiclelayouts.meta',
}
------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Temporario/blazerdeic/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Temporario/blazerdeic/vehicles.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Temporario/blazerdeic/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Temporario/blazerdeic/carvariations.meta'

files {
'data/Temporario/blazerdeic/handling.meta',
'data/Temporario/blazerdeic/vehicles.meta',
'data/Temporario/blazerdeic/carvariations.meta',
'data/Temporario/blazerdeic/vehiclelayouts.meta',
}
------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Temporario/blazergoe/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Temporario/blazergoe/vehicles.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Temporario/blazergoe/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Temporario/blazergoe/carvariations.meta'

files {
'data/Temporario/blazergoe/handling.meta',
'data/Temporario/blazergoe/vehicles.meta',
'data/Temporario/blazergoe/carvariations.meta',
'data/Temporario/blazergoe/vehiclelayouts.meta',
}
------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/Temporario/blazerprf/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Temporario/blazerprf/vehicles.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Temporario/blazerprf/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Temporario/blazerprf/carvariations.meta'

files {
'data/Temporario/blazerprf/handling.meta',
'data/Temporario/blazerprf/vehicles.meta',
'data/Temporario/blazerprf/carvariations.meta',
'data/Temporario/blazerprf/vehiclelayouts.meta',
}
------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/Temporario/dakarprf/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Temporario/dakarprf/vehicles.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Temporario/dakarprf/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Temporario/dakarprf/carvariations.meta'

files {
'data/Temporario/dakarprf/handling.meta',
'data/Temporario/dakarprf/vehicles.meta',
'data/Temporario/dakarprf/carvariations.meta',
'data/Temporario/dakarprf/vehiclelayouts.meta',
}
------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/Temporario/evolutionPC/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Temporario/evolutionPC/vehicles.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Temporario/evolutionPC/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Temporario/evolutionPC/carvariations.meta'

files {
'data/Temporario/evolutionPC/handling.meta',
'data/Temporario/evolutionPC/vehicles.meta',
'data/Temporario/evolutionPC/carvariations.meta',
'data/Temporario/evolutionPC/vehiclelayouts.meta',
}
------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/Temporario/fusionprf/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Temporario/fusionprf/vehicles.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Temporario/fusionprf/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Temporario/fusionprf/carvariations.meta'

files {
'data/Temporario/fusionprf/handling.meta',
'data/Temporario/fusionprf/vehicles.meta',
'data/Temporario/fusionprf/carvariations.meta',
'data/Temporario/fusionprf/vehiclelayouts.meta',
}
------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/Temporario/heliprf/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Temporario/heliprf/vehicles.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Temporario/heliprf/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Temporario/heliprf/carvariations.meta'

files {
'data/Temporario/heliprf/handling.meta',
'data/Temporario/heliprf/vehicles.meta',
'data/Temporario/heliprf/carvariations.meta',
'data/Temporario/heliprf/vehiclelayouts.meta',
}
------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/Temporario/l200PM/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Temporario/l200PM/vehicles.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Temporario/l200PM/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Temporario/l200PM/carvariations.meta'

files {
'data/Temporario/l200PM/handling.meta',
'data/Temporario/l200PM/vehicles.meta',
'data/Temporario/l200PM/carvariations.meta',
'data/Temporario/l200PM/vehiclelayouts.meta',
}
------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/Temporario/nissanprf/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Temporario/nissanprf/vehicles.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Temporario/nissanprf/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Temporario/nissanprf/carvariations.meta'

files {
'data/Temporario/nissanprf/handling.meta',
'data/Temporario/nissanprf/vehicles.meta',
'data/Temporario/nissanprf/carvariations.meta',
'data/Temporario/nissanprf/vehiclelayouts.meta',
}
------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/Temporario/pajeroPC/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Temporario/pajeroPC/vehicles.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Temporario/pajeroPC/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Temporario/pajeroPC/carvariations.meta'

files {
'data/Temporario/pajeroPC/handling.meta',
'data/Temporario/pajeroPC/vehicles.meta',
'data/Temporario/pajeroPC/carvariations.meta',
'data/Temporario/pajeroPC/vehiclelayouts.meta',
}
------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/Temporario/fiestadesc/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Temporario/fiestadesc/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Temporario/fiestadesc/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Temporario/fiestadesc/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Temporario/fiestadesc/carvariations.meta'

files {
'data/Temporario/fiestadesc/handling.meta',
'data/Temporario/fiestadesc/vehicles.meta',
'data/Temporario/fiestadesc/carcols.meta',
'data/Temporario/fiestadesc/carvariations.meta',
'data/Temporario/fiestadesc/vehiclelayouts.meta',
}
---------------------------------------------------------------------



client_script 'vehicle_names.lua'